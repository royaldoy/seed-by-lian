const axios = require("axios");
const fs = require("fs");
async function fetchClaim(teleId) {
  try {
    const response = await axios.post(
      "https://elb.seeddao.org/api/v1/seed/claim",
      null,
      {
        headers: {
          accept: "application/json, text/plain, */*",
          "accept-language": "en-US,en;q=0.9",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          "telegram-data": teleId,
          Referer: "https://cf.seeddao.org/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    return response.data;
  } catch (error) {
    return false;
    console.error("Error fetching claim:", error);
  }
}

async function fetchBalance(teleId) {
  try {
    const response = await axios.get(
      "https://elb.seeddao.org/api/v1/profile/balance",
      {
        headers: {
          accept: "application/json, text/plain, */*",
          "accept-language": "en-US,en;q=0.9",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          "telegram-data": teleId,
          Referer: "https://cf.seeddao.org/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    return response.data;
  } catch (error) {
    return false;
    console.error("Error fetching balance:", error);
  }
}

async function fetchProfile(teleId) {
  try {
    const response = await axios.get("https://elb.seeddao.org/api/v1/profile", {
      headers: {
        accept: "application/json, text/plain, */*",
        "accept-language": "en-US,en;q=0.9",
        priority: "u=1, i",
        "sec-ch-ua":
          '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"Windows"',
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "telegram-data": teleId,
        Referer: "https://cf.seeddao.org/",
        "Referrer-Policy": "strict-origin-when-cross-origin",
      },
    });
    return response.data.data;
    // console.log();
  } catch (error) {
    return false;
    console.error("Error fetching profile:", error);
  }
}

async function upgradeMiningSpeed(teleId) {
  try {
    const response = await axios.post(
      "https://elb.seeddao.org/api/v1/seed/mining-speed/upgrade",
      null,
      {
        headers: {
          accept: "application/json, text/plain, */*",
          "accept-language": "en-US,en;q=0.9",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          "telegram-data": teleId,
          Referer: "https://cf.seeddao.org/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );

    return "Berhasil Upgrade Pohon!";
    console.log(response.data);
  } catch (error) {
    return "Gagal Upgrade Pohon!";
    console.error("Error upgrading mining speed:", error);
  }
}
async function fetchLoginBonuses(teleId) {
  try {
    const response = await axios.post(
      "https://elb.seeddao.org/api/v1/login-bonuses",
      null, // No body content
      {
        headers: {
          accept: "application/json, text/plain, */*",
          "accept-language": "en-US,en;q=0.9",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          "telegram-data": teleId,
          Referer: "https://cf.seeddao.org/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    // console.log(response.data);
    return response.data;
  } catch (error) {
    return false;
    console.error("Error fetching login bonuses:", error);
  }
}

async function fetchTasks(teleId) {
  const headers = {
    accept: "application/json, text/plain, */*",
    "accept-language": "en-US,en;q=0.9",
    priority: "u=1, i",
    "sec-ch-ua":
      '"Chromium";v="124", "Microsoft Edge";v="124", "Not-A.Brand";v="99", "Microsoft Edge WebView2";v="124"',
    "sec-ch-ua-arch": '"x86"',
    "sec-ch-ua-bitness": '"64"',
    "sec-ch-ua-full-version": '"124.0.2478.97"',
    "sec-ch-ua-full-version-list":
      '"Chromium";v="124.0.6367.201", "Microsoft Edge";v="124.0.2478.97", "Not-A.Brand";v="99.0.0.0", "Microsoft Edge WebView2";v="124.0.2478.97"',
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-model": '""',
    "sec-ch-ua-platform": '"Windows"',
    "sec-ch-ua-platform-version": '"15.0.0"',
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site",
    "telegram-data": teleId,
    cookie:
      "cf_clearance=YJh50JXGeYgCg1o9BMwWMIhhksEg167mzTi354jBfzc-1716287756-1.0.1.1-ApW.EUCyUd5efDHhfEkBf6U6Suu9Moon2YON3dWE133FUYge.oUoUvhgVq4sPa3lsYhM8e6Os78CKrP5w2KEhQ",
    Referer: "https://elb.seeddao.org/missions",
    "Referrer-Policy": "strict-origin-when-cross-origin",
  };

  const urls = [
    "https://elb.seeddao.org/api/v1/tasks/5007a283-7837-4a15-a6ee-c94236656551",
    "https://elb.seeddao.org/api/v1/tasks/bd9361da-2862-472a-af4d-7835caa17eae",
    "https://elb.seeddao.org/api/v1/tasks/a72d528a-b019-4d20-9a5a-b4fdba52fe4a",
    "https://elb.seeddao.org/api/v1/tasks/7fdc46b3-6612-453a-9ef7-05471800f0ad",
    "https://elb.seeddao.org/api/v1/tasks/e3b5e992-8b0a-4cb0-8a70-ca317fbc758e",
    "https://elb.seeddao.org/api/v1/tasks/313f4c37-4a1e-4921-bc28-589857740d14",
    "https://elb.seeddao.org/api/v1/tasks/51571b38-0a73-41fe-ba78-563ce8f92878",
    "https://elb.seeddao.org/api/v1/tasks/299bb87d-40e3-485b-8083-67bba87b3520",
  ];
  let a = "Gagal Klaim";
  for (const url of urls) {
    try {
      const response = await axios.post(url, null, { headers });
      a = "Berhasil Klaim!";
    } catch (error) {
      // console.error(`Error fetching task from ${url}:`, error);
      a = "Gagal Klaim";
    }
  }
  return a;
}

async function doRunBot() {
  process.stdout.write("\x1Bc");
  var file = fs.readFileSync("tgid.txt", "utf-8");
  var splitFile = file.split("\r\n");
  //   var splitFile = file.split("\n");

  for (let i = 0; i < splitFile.length; i++) {
    const profile = await fetchProfile(splitFile[i]);
    const balance = await fetchBalance(splitFile[i]);
    const claim = await fetchClaim(splitFile[i]);
    const claimBonuses = await fetchLoginBonuses(splitFile[i]);
    const selesaiTask = await fetchTasks(splitFile[i]);
    const pohon = await upgradeMiningSpeed(splitFile[i]);

    const nama = profile.name;
    const claimed = claim ? "berhasil klaim" : "gagal claim";
    const daily = claimBonuses ? "berhasil klaim daily" : "gagal claim daily";
    const bal = balance.data / 1000000000;

    console.log(
      `${nama} | ${bal} | ${claimed} | ${daily} | ${selesaiTask} Task | ${pohon}`
    );
  }
}

doRunBot();
// fetchLoginBonuses("");
